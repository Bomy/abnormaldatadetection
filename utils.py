#-*- coding:utf-8 -*-
import numpy as npy

def mnormal(narray):
    """Make the narray's element value in 0 and 1

    :type narray: numpy array
    :rtype: numpy array
    :formula: result = (oldvalue - min)/(max - min)
    """
    rowsize = narray.shape[0]
    celsize = narray.shape[1]
    minvals = narray.min(0)
    maxvals = narray.max(0)
    
    subarr = narray-npy.tile(minvals,(rowsize,1))
    subvals = maxvals-minvals
    res = subarr/npy.tile(subvals,(rowsize,1))
    return res

def mstandard(narray):
    """Since there is a great difference in some data and some data has little influence on the result ,we need to make those data  more equitable
    :type narray: numpy array
    :rtype: numpy array
    formula: result = (oldval - meanval)/standard_deviation
    """
    rowsize = narray.shape[0]
    celsize = narray.shape[1]
    meanvals = narray.mean(0)
    stdvals = narray.std(0)
    subvals = narray - npy.tile(meanvals,(rowsize,1))
    res = subvals/npy.tile(stdvals,(rowsize,1))
    return res

def cossimilar(marray,tarr,*arg):
    """Calculate cos similarity

    :type marray:numpy array,data_fragment
    :type tarr:numpy array,data_fragment
    :rtype: numpy array
    :formula: cos = (x1*y1_x2*y2+...+xn*yn)/(√(x1^2+x2^2+...+xn^2)+√(y1^2+y2^2+...+yn^2))
    """

    rowsize = marray.shape[0]
    #fullarr = zeros(tarr,(rowsize,axis = 0))
    subvals = marray - npy.tile(tarr,(rowsize,1))
    expvlas = npy.sum(marray**2,axis=1)
    texpvals = npy.sum(npy.tile(tarr**2,(rowsize,1)),axis=1)
    multvals = marray*npy.tile(tarr,(rowsize,1))
    addvals = npy.sum(multvals,axis=1)
    res = npy.abs(addvals/(texpvals**0.5+texpvals**0.5))
    return res

def eudist(marray,tarr,*arg):
    """Calculate the euclidean diatance 
    :type marray:numpy array,data_fragment
    :type tarr:numpy array,data_fragment
    :rtype:numpy array
    formula:res = √((x1-y1)^2+(x2-y2)^2+...+(xn-yn)^2)
    """

    rowsize = marray.shape[0]
    subvals = marray-npy.tile(tarr,(rowsize,1))
    expvals = subvals**2
    addvals = expvals.sum(axis=1)
    res = addvals**0.5
    return res
